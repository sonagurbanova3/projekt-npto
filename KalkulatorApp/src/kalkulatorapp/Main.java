/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package kalkulatorapp;

/**
 * główna klasa aplikacji
 * @author HP
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Kalkulator kalk = new Kalkulator();
        double a = kalk.dodaj(3, 7.5);
        double b = kalk.odejmij(7, 21);
        double c = kalk.pomnoz(234, 1.5);
        double d = kalk.podziel(c, 3.24);
        System.out.println(a + " " + b + " " + c + " " + d);
    }

}
